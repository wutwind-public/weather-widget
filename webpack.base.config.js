// webpack config for IDE resolving paths

module.exports = () => {
  return {
    resolve: {
      extensions: ['.vue', '.js'],
      alias: {
        vue$: 'vue/dist/vue.runtime.esm-bundler.js',
      },
    },
  };
};
