## WeatherWidget

Widget show weather in selected cities.

## Local Development Environment (LDE)

LDE use Docker for launch development environment

requirements:
- Docker
- Docker Compose
- Traefik

Commands:

- `lde init` - prepare lde (copy required files, build images)
- `lde up` - up docker containers
- `lde down` - down docker containers
- `lde run <SERVICE> <COMMAND>` - run command

---

You may develop app without Docker. Use command:

- `npm ci` - for install required dependencies
- `npm run serve` - for serve app by webpack dev server
- `npm run build` - for build application
