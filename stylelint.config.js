module.exports = {
  fix: true,
  extends: [
    'stylelint-config-recommended-scss',
    'stylelint-config-recommended-vue/scss',
    'stylelint-config-rational-order',
  ],
  plugins: [
    'stylelint-scss',
    'stylelint-order',
    'stylelint-config-rational-order/plugin',
  ],
  rules: {
    'linebreaks': 'unix',
    'plugin/rational-order': [true, {
      'border-in-box-model': false,
      'empty-line-between-groups': true,
    }],

    'rule-empty-line-before': ['always', {
      except: ['after-single-line-comment', 'first-nested'],
    }],

    'declaration-no-important': true,

    'number-leading-zero': 'always',

    'selector-list-comma-newline-after': 'always',
    'selector-list-comma-newline-before': 'never-multi-line',

    'color-named': 'never',
    'color-no-invalid-hex': true,
    'color-hex-case': 'lower',
    'color-hex-length': 'long',

    'indentation': 2,
    'max-empty-lines': 1,
    'max-line-length': [
      100,
      {
        severity: 'warning',
      },
    ],
  },
};
