module.exports = {
  root: true,

  env: {
    browser: true,
    es6: true,
    node: true,
  },

  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@babel/eslint-parser',
  },

  extends: [
    'eslint:recommended',
    'plugin:import/recommended',
    'plugin:vue/vue3-recommended',
  ],
  plugins: [
    'import',
    'vue',
  ],
  globals: {
    __ENV__: true,
  },
  rules: {
    'camelcase': ['off', {
      properties: 'never',
    }],
    'indent': ['error',
      2,
      { SwitchCase: 1 },
    ],
    'linebreak-style': ['error', 'unix'],
    'quote-props': ['error', 'consistent-as-needed'],
    'quotes': ['error', 'single'],
    'semi': ['error', 'always'],

    'comma-dangle': ['error', {
      arrays: 'always-multiline',
      objects: 'always-multiline',
      imports: 'always-multiline',
      exports: 'always-multiline',
      functions: 'always-multiline',
    }],
    'comma-spacing': 'error',

    'no-var': 'error',
    'one-var': ['error', {
      initialized: 'never',
      uninitialized: 'always',
    }],

    'array-bracket-spacing': ['error', 'never', {
      singleValue: true,
    }],
    'array-callback-return': 'error',
    'object-curly-spacing': ['error', 'always'],
    'object-shorthand': ['error', 'always'],

    'prefer-const': ['error', {
      destructuring: 'any',
      ignoreReadBeforeAssign: false,
    }],
    'prefer-destructuring': ['error', {
      VariableDeclarator: {
        array: false,
        object: true,
      },
    }],
    'prefer-rest-params': 'error',

    'eqeqeq': ['error', 'always'],
    'no-case-declarations': 'error',
    'no-else-return': ['error', {
      allowElseIf: false,
    }],
    'no-unneeded-ternary': 'error',

    'consistent-return': 'error',
    'newline-per-chained-call': ['error', {
      ignoreChainWithDepth: 2,
    }],

    'no-confusing-arrow': ['error', {
      allowParens: true,
    }],
    'no-dupe-class-members': 'error',
    'no-duplicate-imports': 'error',

    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',

    'arrow-parens': ['error', 'as-needed'],
    'prefer-arrow-callback': ['error', {
      allowNamedFunctions: true,
    }],
    'space-before-function-paren': ['error', {
      anonymous: 'never',
      named: 'never',
    }],

    'no-multi-assign': 'error',

    /* vue rules */
    'vue/html-indent': ['error', 4],
    'vue/match-component-file-name': ['error', {
      extensions: ['jsx', 'vue'],
      shouldMatchCase: true,
    }],
  },
};
