import { DefinePlugin } from 'webpack';

export const definePlugin = () => ({
  plugins: [
    new DefinePlugin({
      __ENV__: JSON.stringify(process.env.NODE_ENV),
      __VUE_OPTIONS_API__: true,
      __VUE_PROD_DEVTOOLS__: false,
    }),
  ],
});
