import HtmlWebpackPlugin from 'html-webpack-plugin';

export const htmlPlugin = () => ({
  plugins: [
    new HtmlWebpackPlugin({
      // favicon: 'src/favicon.ico',
      filename: 'index.html',
      template: 'src/index.html',
      inject: true,
    }),
  ],
});
