import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';

export const bundleAnalyzerPlugin = () => ({
  plugins: [
    new BundleAnalyzerPlugin({
      analyzerMode: 'disabled',
      generateStatsFile: true,
      openAnalyzer: false,
    }),
  ],
});
