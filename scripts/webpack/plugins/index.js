export { htmlPlugin } from './htmlPlugin';
export { vuePlugin } from './vuePlugin';
export { definePlugin } from './definePlugin';
export { miniCssExtractPlugin } from './miniCssExtractPlugin';
export { bundleAnalyzerPlugin } from './bundleAnalyzerPlugin';
