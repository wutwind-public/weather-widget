import { merge } from 'webpack-merge';
import { webpack } from 'webpack';

import {
  getWebpackBaseConfig,
  getWebpackDevConfig,
  getWebpackProdConfig,
} from '../configs';

const configsMap = new Map([
  ['production', getWebpackProdConfig()],
  ['development', getWebpackDevConfig()],
  [undefined, getWebpackProdConfig()],
]);

export const getWebpackCompiler = () => {
  const config = merge(
    getWebpackBaseConfig(),
    configsMap.get(process.env.NODE_ENV),
  );

  const compiler = webpack(config);

  compiler.hooks.beforeRun.tap({ name: 'start' }, () => {
    console.log('compilation start');
  });

  compiler.hooks.done.tap({ name: 'done' }, () => {
    console.log('compilation completed');
  });

  return compiler;
};
