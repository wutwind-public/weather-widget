import dotenv from 'dotenv';
import WebpackDevServer from 'webpack-dev-server';
import { getWebpackCompiler } from './utils/compiler';

dotenv.config();

const webpackCompiler = getWebpackCompiler();

const devServerOptions = {
  allowedHosts: 'all',
  compress: true,
  historyApiFallback: true,
  hot: true,

  client: {
    overlay: {
      errors: true,
      warnings: false,
    },
    webSocketURL: {
      hostname: '0.0.0.0',
      pathname: '/ws',
      port: process.env.PROJECT_LOCALHOST_PORT ?? '80',
    },
  },
  webSocketServer: 'sockjs',
};

const server = new WebpackDevServer(devServerOptions, webpackCompiler);

server.start();
