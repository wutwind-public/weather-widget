export { vueLoader } from './vueLoader';
export { jsLoader } from './jsLoader';
export { cssLoaderDev } from './cssLoaderDev';
export { cssLoaderProd } from './cssLoaderProd';
export { sassLoaderDev } from './sassLoaderDev';
export { sassLoaderProd } from './sassLoaderProd';
