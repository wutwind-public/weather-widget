import { merge } from 'webpack-merge';
//
import * as loaders from '../loaders';
// import * as plugins from '../modules/plugins';

export const getWebpackDevConfig = () => {
  const devConfig = {
    entry: {
      app: './src/main.js',
    },
    mode: 'development',
    devtool: 'eval-cheap-module-source-map',
  };

  return merge(
    devConfig,

    loaders.cssLoaderDev(),
    loaders.sassLoaderDev(),
    // loaders.sassLoaderDev(),
    // plugins.hotReplacePlugin()
  );
};
