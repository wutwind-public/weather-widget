import { merge } from 'webpack-merge';

import * as loaders from '../loaders';
import * as plugins from '../plugins';
import * as optimize from '../optimization';

export const getWebpackProdConfig = () => {
  return merge(
    {
      mode: 'production',
    },
    /* loaders */
    loaders.cssLoaderProd(),
    loaders.sassLoaderProd(),

    /* plugins */
    plugins.miniCssExtractPlugin(),
    plugins.bundleAnalyzerPlugin(),

    /* optimization */
    optimize.buildOptimize(),
    // optimize.filterMomentLocale(),

  );
};
