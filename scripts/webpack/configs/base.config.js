import appRootPath from 'app-root-path';

import * as loaders from '../loaders';
import * as plugins from '../plugins';

import { merge } from 'webpack-merge';

const getWebpackRootConfig = require(appRootPath.resolve('webpack.base.config.js'));

export const getWebpackBaseConfig = () => {
  const baseConfig = {
    entry: {
      app: appRootPath.resolve('src/main.js'),
    },
    module: {
      noParse: /^(vue|vue-router|vuex|vuex-router-sync)$/,
    },
    output: {
      clean: true,
      path: appRootPath.resolve('dist'),
      publicPath: '/',

      assetModuleFilename: 'assets/[hash][ext]',
      chunkFilename: 'assets/js/[name].[chunkhash:8].js',
      filename: 'assets/js/[name].[contenthash:8].js',
    },
    stats: {
      colors: true,
      env: true,
      hash: true,
      logging: 'info',
      modules: false,
    },
  };

  return merge(
    getWebpackRootConfig(),
    baseConfig,

    plugins.htmlPlugin(),
    plugins.vuePlugin(),
    plugins.definePlugin(),

    loaders.vueLoader(),
    loaders.jsLoader(),
  );
};
